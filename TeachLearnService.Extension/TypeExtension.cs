﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace TeachLearnService.Extension
{
    public static class TypeExtension
    {
        public static T MapTo<T>(this object source)
        {
            Type targettype = typeof(T);
            Type sourcetype = source.GetType();

            T result = Activator.CreateInstance<T>();

            PropertyInfo[] targetproperties = targettype.GetProperties();
            PropertyInfo[] sourceproperties = sourcetype.GetProperties();

            foreach (PropertyInfo sp in sourceproperties)
            {
                PropertyInfo tp = targetproperties.FirstOrDefault(x => x.Name.ToLower() == sp.Name.ToLower());

                if (tp != null)
                {
                    object data = sp.GetValue(source);
                    tp.SetValue(result, data);
                }
            }
            return result;
        }
    }
}
