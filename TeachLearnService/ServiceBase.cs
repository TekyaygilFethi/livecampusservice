﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using System.Web;
using TeachLearnService.Business.Repository;
using TeachLearnService.Extension;

namespace TeachLearnService
{
    public class ServiceBase<Rep, Entity, DTO> : IServiceBase<DTO,Entity> where Rep : IRepository<Entity> where Entity : class
    {
        private static Rep _repository;

        public static Rep Repository
        {
            get
            {
                if (_repository == null)
                { _repository = Activator.CreateInstance<Rep>(); }
                return _repository;
            }
            set { _repository = value; }
        }

        public async Task AddAsync(DTO _item)
        {
            await Repository.AddAsync(_item.MapTo<Entity>());
        }

        public async Task DeleteAsync(DTO _deletedItem)
        {
            await Repository.DeleteAsync(_deletedItem.MapTo<Entity>());
        }

        public async Task<IEnumerable<DTO>> GetAllAsync()
        {
            var resultSet = await Repository.GetAllAsync();
            return resultSet.Select(x => x.MapTo<DTO>()).ToList();
        }

        public async Task<DTO> GetAsync(int ID)
        {
            return Repository.GetAsync(ID).MapTo<DTO>();
        }

        public async Task<IEnumerable<DTO>> GetByAsync(Func<Entity, bool> predicate)
        {
            var list= await Repository.GetByAsync(predicate);
            List<DTO> newList = new List<DTO>();

            foreach(var item in list)
            {
                newList.Add(item.MapTo<DTO>());
            }
            return newList.AsEnumerable();
        }

        public async Task<DTO> SingleGetByAsync(Expression<Func<Entity, bool>> predicate)
        {
            var _entity = await Repository.SingleGetByAsync(predicate);
            return _entity.MapTo<DTO>();
        }

        public async Task UpdateAsync(DTO _newItem)
        {
            await Repository.UpdateAsync(_newItem.MapTo<Entity>());
        }
    }
}