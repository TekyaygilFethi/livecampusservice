﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using TeachLearnWeb.Data.POCOs.TeachLearnWeb;
using TeachLearnWeb.Data;
using TeachLearnWeb.Data.DbContextFolder;
using System.Web.Helpers;
using System.Threading.Tasks;

namespace TeachLearnService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "DataTransferService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select DataTransferService.svc or DataTransferService.svc.cs at the Solution Explorer and start debugging.
    public class DataTransferService : IDataTransferService
    {
        public string ReturnViewerIP(int ID)
        {
            using (TeachLearnWebDbContext entities = new TeachLearnWebDbContext())
            {
                Connection _connection = entities.Connections.SingleOrDefault(w => w.ID == ID);

                return _connection.ViewerIP;
            }
        }

        public string ReturnSharerIP(int ID)
        {
            using (TeachLearnWebDbContext entities = new TeachLearnWebDbContext())
            {
                Connection _connection = entities.Connections.SingleOrDefault(w => w.ID == ID);

                return _connection.SharerIP;
            }
        }

        Connection IDataTransferService.DesktopToWeb(Connection _data)
        {
            using (TeachLearnWebDbContext entities = new TeachLearnWebDbContext())
            {
                Connection _currentConnection = entities.Connections.FirstOrDefault(w => w.ID == _data.ID);

                _currentConnection.ConnectionString = _data.ConnectionString;
                _currentConnection.ViewerIP = _data.ViewerIP;
                _currentConnection.SharerIP = _data.SharerIP;

                entities.SaveChanges();

                return _currentConnection;
            }
        }

        Connection IDataTransferService.WebToDesktop(int ID)
        {
            string param = ID.ToString();

            using (TeachLearnWebDbContext entities = new TeachLearnWebDbContext())
            {

                Connection _connection = entities.Connections.SingleOrDefault(w => w.ID == ID);

                return _connection;
            }
        }

        public int CheckLoginCreedientals(string username, string password)
        {
            using (TeachLearnWebDbContext entities = new TeachLearnWebDbContext())
            {
                User _currentUser = entities.Users.SingleOrDefault(w => w.Username == username);

                if (_currentUser != null)
                {
                    var _userEnteredPassword = password + _currentUser.Salt;

                    if (Crypto.VerifyHashedPassword(_currentUser.Password, _userEnteredPassword))
                    {
                        return _currentUser.ID;
                    }
                    else
                    {
                        return -1;
                    }
                }
                else
                {
                    return -1;
                }

            }
        }

        public Connection ControlConnectionForCreation(int userID) //random bir connection bul ve kullanıcıyı ona ekle
        {
            using (TeachLearnWebDbContext entities = new TeachLearnWebDbContext())
            {
                List<Connection> connectionList = new List<Connection>();
                User user = entities.Users.SingleOrDefault(w => w.ID == userID);
                string pendingStatus = user.PendingStatus;

                if (pendingStatus == "Viewer")
                {
                    connectionList.AddRange(entities.Connections.Where(w => w.Viewer == null && w.Sharer != null));
                }

                if (pendingStatus == "Sharer")
                {
                    connectionList.AddRange(entities.Connections.Where(w => w.Viewer != null && w.Sharer == null));
                }

                else
                    return null;

                int randomNumber = FakeData.NumberData.GetNumber(1, connectionList.Count - 1);

                Connection randConnection = connectionList.ElementAt(randomNumber);

                if (pendingStatus == "Viewer")
                {
                    randConnection.Viewer = user;
                    user.Connections.Add(randConnection);
                }

                if (pendingStatus == "Sharer")
                {
                    randConnection.Sharer = user;
                    user.Connections.Add(randConnection);
                }

                entities.SaveChanges();

                return randConnection;
            }
        }

        public Connection CreateConnection(int ID)
        {
            using (TeachLearnWebDbContext entities = new TeachLearnWebDbContext())
            {
                User _currentUser = entities.Users.SingleOrDefault(w => w.ID == ID);
                Connection _newConnection = new Connection();

                switch (_currentUser.PendingStatus)
                {
                    case "Viewer":
                        _newConnection.Viewer = _currentUser;
                        break;

                    case "Sharer":
                        _newConnection.Sharer = _currentUser;
                        break;

                    default:
                        return null;
                }

                entities.Users.Attach(_currentUser);
                _currentUser.Connections.Add(_newConnection);
                entities.Connections.Add(_newConnection);

                entities.SaveChanges();

                return _newConnection;
            }
        }

        public bool ControlConnectionForJoin(string pendingStatus, int connectionID)
        {
            using (TeachLearnWebDbContext entities = new TeachLearnWebDbContext())
            {
                Connection conn = entities.Connections.SingleOrDefault(w => w.ID == connectionID);
                if(pendingStatus=="Viewer" && conn.Sharer!=null)
                {
                    return true;
                }
                else if(pendingStatus=="Sharer" && conn.Viewer!=null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public Connection SaveInvitationString(int connectionID, string InvitationString)
        {
            using (TeachLearnWebDbContext entities = new TeachLearnWebDbContext())
            {
                Connection _currentConnection = entities.Connections.SingleOrDefault(w => w.ID == connectionID);
                _currentConnection.ConnectionString = InvitationString;

                entities.SaveChanges();

                return _currentConnection;
            }
        }

        public string GetInvitationString(int connectionID)
        {
            using (TeachLearnWebDbContext entities = new TeachLearnWebDbContext())
            {
                return entities.Connections.SingleOrDefault(w => w.ID == connectionID).ConnectionString;
            }
        }
    }
}
