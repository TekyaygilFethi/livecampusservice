﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using TeachLearnService.Data.POCOs;
using TeachLearnWeb.Data.POCOs.TeachLearnWeb;

namespace TeachLearnService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IDataTransferService" in both code and config file together.
    [ServiceContract]
    public interface IDataTransferService
    {
        [OperationContract]
        Connection DesktopToWeb(Connection _data);

        [OperationContract]
        Connection WebToDesktop(int ID);

        [OperationContract]
        string ReturnViewerIP(int ID);

        [OperationContract]
        string ReturnSharerIP(int ID);

        [OperationContract]
        int CheckLoginCreedientals(string username, string password);

        [OperationContract]
        Connection ControlConnectionForCreation(int userID);

        [OperationContract]
        bool ControlConnectionForJoin(string pendingStatus, int connectionID);

        [OperationContract]
        Connection CreateConnection(int ID);

        [OperationContract]
        Connection SaveInvitationString(int connectionID, string InvitationString);

        [OperationContract]
        string GetInvitationString(int connectionID);
    }
}
