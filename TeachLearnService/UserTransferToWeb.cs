﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace TeachLearnService
{
    [DataContract]
    public class UserTransferToWeb
    {
        [DataMember]
        public int SharerID { get; set; }

        [DataMember]
        public int ViewerID { get; set; }
    }
}