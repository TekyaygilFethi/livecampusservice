namespace TeachLearnService.DTO.TeachLearnDTO
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public partial class PeriodTableDTO
    {

        public PeriodTableDTO()
        {
            EducationTablesDTOs = new HashSet<EducationTableDTO>();
            LessonTablesDTOs = new HashSet<LessonTableDTO>();
        }

        public int ID { get; set; }

        public int Year { get; set; }

        public int Semester { get; set; }


        public virtual ICollection<EducationTableDTO> EducationTablesDTOs { get; set; }


        public virtual ICollection<LessonTableDTO> LessonTablesDTOs { get; set; }
    }
}
