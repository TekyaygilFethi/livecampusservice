namespace TeachLearnService.DTO.TeachLearnDTO
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public partial class MessageTableDTO
    {
        public int ID { get; set; }

        public string MessageContent { get; set; }

        public DateTime SendTime { get; set; }

        public bool IsPinned { get; set; }

        public int UserID { get; set; }

        public int ConversationID { get; set; }

        public virtual ConversationTableDTO ConversationTableDTO { get; set; }

        public virtual UserTableDTO UserTableDTO { get; set; }
    }
}
