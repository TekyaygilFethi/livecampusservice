namespace TeachLearnService.DTO.TeachLearnDTO
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public partial class LessonTableDTO
    {
        public LessonTableDTO()
        {
            EducationTablesDTOs = new HashSet<EducationTableDTO>();
            ScoreTablesDTOs = new HashSet<ScoreTableDTO>();
        }

        public int ID { get; set; }

        public string Name { get; set; }

        public int DepartmentID { get; set; }

        public int? PeriodID { get; set; }

        public virtual DepartmentTableDTO DepartmentTableDTO { get; set; }
        
        public virtual ICollection<EducationTableDTO> EducationTablesDTOs { get; set; }

        public virtual PeriodTableDTO PeriodTableDTO { get; set; }
        
        public virtual ICollection<ScoreTableDTO> ScoreTablesDTOs { get; set; }
    }
}
