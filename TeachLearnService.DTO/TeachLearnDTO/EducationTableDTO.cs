namespace TeachLearnService.DTO.TeachLearnDTO
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    public partial class EducationTableDTO
    {
        public EducationTableDTO()
        {
            ScoreTablesDTOs = new HashSet<ScoreTableDTO>();
        }

        public int ID { get; set; }

        public int UniversityID { get; set; }

        public int FacultyID { get; set; }

        public int DepartmentID { get; set; }

        public int StudentID { get; set; }

        public int PeriodID { get; set; }

        public int? Lesson_ID { get; set; }

        public virtual DepartmentTableDTO DepartmentTableDTO { get; set; }

        public virtual FacultyTableDTO FacultyTableDTO { get; set; }

        public virtual LessonTableDTO LessonTableDTO { get; set; }

        public virtual PeriodTableDTO PeriodTableDTO { get; set; }

        public virtual StudentTableDTO StudentTableDTO { get; set; }

        public virtual UniversityTableDTO UniversityTableDTO { get; set; }
        
        public virtual ICollection<ScoreTableDTO> ScoreTablesDTOs { get; set; }
    }
}
