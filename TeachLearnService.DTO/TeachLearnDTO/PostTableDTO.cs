namespace TeachLearnService.DTO.TeachLearnDTO
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public partial class PostTableDTO
    {
        public int ID { get; set; }

        public string Content { get; set; }

        public byte[] Attachment { get; set; }

        public int UserID { get; set; }

        public DateTime PostTime { get; set; }

        public int TopicID { get; set; }

        public int? SentFeeds_ID { get; set; }

        public int? FavouriteFeeds_ID { get; set; }

        public virtual FavouriteFeedsTableDTO FavouriteFeedsTableDTO { get; set; }

        public virtual SentFeedsTableDTO SentFeedsTableDTO { get; set; }

        public virtual TopicTableDTO TopicTableDTO { get; set; }

        public virtual UserTableDTO UserTableDTO { get; set; }
    }
}
