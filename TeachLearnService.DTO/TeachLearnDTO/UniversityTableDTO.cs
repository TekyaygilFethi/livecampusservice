namespace TeachLearnService.DTO.TeachLearnDTO
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public partial class UniversityTableDTO
    {

        public UniversityTableDTO()
        {
            DepartmentTablesDTOs = new HashSet<DepartmentTableDTO>();
            EducationTablesDTOs = new HashSet<EducationTableDTO>();
            FacultyTablesDTOs = new HashSet<FacultyTableDTO>();
        }

        public int ID { get; set; }

        public string Name { get; set; }


        public virtual ICollection<DepartmentTableDTO> DepartmentTablesDTOs { get; set; }


        public virtual ICollection<EducationTableDTO> EducationTablesDTOs { get; set; }


        public virtual ICollection<FacultyTableDTO> FacultyTablesDTOs { get; set; }
    }
}
