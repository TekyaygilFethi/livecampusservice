namespace TeachLearnService.DTO.TeachLearnDTO
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public partial class StudentTableDTO
    {

        public StudentTableDTO()
        {
            EducationTablesDTOs = new HashSet<EducationTableDTO>();
        }


        public int ID { get; set; }

        public string Name { get; set; }

        public string Surname { get; set; }

        public DateTime Birthday { get; set; }


        public virtual ICollection<EducationTableDTO> EducationTablesDTOs { get; set; }

        public virtual UserTableDTO UserTableDTO { get; set; }
    }
}
