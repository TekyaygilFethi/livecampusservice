namespace TeachLearnService.DTO.TeachLearnDTO
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public partial class ReviewTableDTO
    {
        public int ID { get; set; }

        public string Message { get; set; }

        public int ReviewPoint { get; set; }

        public int UserID { get; set; }

        public virtual UserTableDTO UserTableDTOs { get; set; }
    }
}
