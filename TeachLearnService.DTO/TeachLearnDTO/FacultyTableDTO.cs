namespace TeachLearnService.DTO.TeachLearnDTO
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public partial class FacultyTableDTO
    {
        public FacultyTableDTO()
        {
            DepartmentTablesDTOs = new HashSet<DepartmentTableDTO>();
            EducationTablesDTOs = new HashSet<EducationTableDTO>();
        }

        public int ID { get; set; }

        public string Name { get; set; }

        public int UniversityID { get; set; }
        
        public virtual ICollection<DepartmentTableDTO> DepartmentTablesDTOs { get; set; }
        
        public virtual ICollection<EducationTableDTO> EducationTablesDTOs { get; set; }

        public virtual UniversityTableDTO UniversityTableDTO { get; set; }
    }
}
