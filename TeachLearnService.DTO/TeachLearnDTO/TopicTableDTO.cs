namespace TeachLearnService.DTO.TeachLearnDTO
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public partial class TopicTableDTO
    {

        public TopicTableDTO()
        {
            PostTablesDTOs = new HashSet<PostTableDTO>();
            FavouriteFeedsTablesDTOs = new HashSet<FavouriteFeedsTableDTO>();
        }

        public int ID { get; set; }

        public string Name { get; set; }

        public int SentFeedID { get; set; }


        public virtual ICollection<PostTableDTO> PostTablesDTOs { get; set; }

        public virtual SentFeedsTableDTO SentFeedsTableDTO { get; set; }


        public virtual ICollection<FavouriteFeedsTableDTO> FavouriteFeedsTablesDTOs { get; set; }
    }
}
