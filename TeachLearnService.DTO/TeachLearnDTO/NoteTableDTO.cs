namespace TeachLearnService.DTO.TeachLearnDTO
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public partial class NoteTableDTO
    {
        public int ID { get; set; }

        public string Description { get; set; }

        public double ResultPoint { get; set; }

        public double Effect { get; set; }

        public int ScoreID { get; set; }

        public virtual ScoreTableDTO ScoreTableDTO { get; set; }
    }
}
