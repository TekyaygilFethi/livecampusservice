namespace TeachLearnService.DTO.TeachLearnDTO
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    
    public partial class DepartmentTableDTO
    {
        public DepartmentTableDTO()
        {
            EducationTablesDTOs = new HashSet<EducationTableDTO>();
            LessonTablesDTOs = new HashSet<LessonTableDTO>();
        }

        public int ID { get; set; }

        public string Name { get; set; }

        public int FacultyID { get; set; }

        public int UniversityID { get; set; }

        public virtual FacultyTableDTO FacultyTableDTO { get; set; }

        public virtual UniversityTableDTO UniversityTableDTO { get; set; }
        
        public virtual ICollection<EducationTableDTO> EducationTablesDTOs { get; set; }
        
        public virtual ICollection<LessonTableDTO> LessonTablesDTOs { get; set; }
    }
}
