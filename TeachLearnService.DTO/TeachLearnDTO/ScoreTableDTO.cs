namespace TeachLearnService.DTO.TeachLearnDTO
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;


    public partial class ScoreTableDTO
    {

        public ScoreTableDTO()
        {
            NoteTablesDTOs = new HashSet<NoteTableDTO>();
        }

        public int ID { get; set; }

        public int LessonID { get; set; }

        public int EducationID { get; set; }

        public virtual EducationTableDTO EducationTableDTO { get; set; }

        public virtual LessonTableDTO LessonTableDTO { get; set; }


        public virtual ICollection<NoteTableDTO> NoteTablesDTOs { get; set; }
    }
}
