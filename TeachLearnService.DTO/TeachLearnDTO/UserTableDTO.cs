namespace TeachLearnService.DTO.TeachLearnDTO
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public partial class UserTableDTO
    {

        public UserTableDTO()
        {
            MessageTablesDTOs = new HashSet<MessageTableDTO>();
            PostTablesDTOs = new HashSet<PostTableDTO>();
            ReviewTablesDTOs = new HashSet<ReviewTableDTO>();
            ConversationTablesDTOs = new HashSet<ConversationTableDTO>();
            ConnectionTablesDTOs = new HashSet<ConnectionTableDTO>();
        }

        public int ID { get; set; }
        
        public string Username { get; set; }

        public string Password { get; set; }

        public string Salt { get; set; }

        public string Email { get; set; }

        public bool IsVerified { get; set; }

        public byte[] ProfilePhoto { get; set; }

        public int? SentFeedsID { get; set; }

        public int? FavouriteFeedsID { get; set; }

        public int? Connection_ID { get; set; }

        public virtual ConnectionTableDTO ConnectionTableDTO { get; set; }

        public virtual FavouriteFeedsTableDTO FavouriteFeedsTableDTO { get; set; }
        
        public virtual ICollection<MessageTableDTO> MessageTablesDTOs { get; set; }
        
        public virtual ICollection<PostTableDTO> PostTablesDTOs { get; set; }
        
        public virtual ICollection<ReviewTableDTO> ReviewTablesDTOs { get; set; }

        public virtual SentFeedsTableDTO SentFeedsTableDTO { get; set; }

        public virtual StudentTableDTO StudentTableDTO { get; set; }
        
        public virtual ICollection<ConversationTableDTO> ConversationTablesDTOs { get; set; }
        
        public virtual ICollection<ConnectionTableDTO> ConnectionTablesDTOs { get; set; }
    }
}
