namespace TeachLearnService.DTO.TeachLearnDTO
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    
    public partial class ConversationTableDTO
    {
        public ConversationTableDTO()
        {
            MessageTablesDTOs = new HashSet<MessageTableDTO>();
            UserTablesDTOs = new HashSet<UserTableDTO>();
        }

        public int ID { get; set; }

        public DateTime StartTime { get; set; }
        
        public virtual ICollection<MessageTableDTO> MessageTablesDTOs { get; set; }
        
        public virtual ICollection<UserTableDTO> UserTablesDTOs { get; set; }
    }
}
