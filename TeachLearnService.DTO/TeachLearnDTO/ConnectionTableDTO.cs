namespace TeachLearnService.DTO.TeachLearnDTO
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    
    public partial class ConnectionTableDTO
    {
        public ConnectionTableDTO()
        {
            UserTablesDTOs = new HashSet<UserTableDTO>();
            UserTables1DTOs = new HashSet<UserTableDTO>();
        }

        public int ID { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime FinishedDate { get; set; }

        public TimeSpan SessionTime { get; set; }

        public string ConnectionString { get; set; }

        public string ConnectionName { get; set; }

        public string Password { get; set; }

        public int ParticipatorCount { get; set; }
        
        public virtual ICollection<UserTableDTO> UserTablesDTOs { get; set; }
        
        public virtual ICollection<UserTableDTO> UserTables1DTOs { get; set; }
    }
}
