namespace TeachLearnService.DTO.TeachLearnDTO
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public partial class SentFeedsTableDTO
    {

        public SentFeedsTableDTO()
        {
            PostTablesDTOs = new HashSet<PostTableDTO>();
            TopicTablesDTOs = new HashSet<TopicTableDTO>();
        }


        public int ID { get; set; }


        public virtual ICollection<PostTableDTO> PostTablesDTOs { get; set; }

        public virtual UserTableDTO UserTableDTO { get; set; }


        public virtual ICollection<TopicTableDTO> TopicTablesDTOs { get; set; }
    }
}
