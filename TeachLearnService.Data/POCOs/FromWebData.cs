﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TeachLearnService.Data.POCOs
{
    public class FromWebData
    {
        public int SharerID { get; set; }

        public int ViewerID { get; set; }

        public string SharerNameSurname { get; set; }

        public string ViewerNameSurname { get; set; }

        public string InvitationString { get; set; }

        public string InvitationPassword { get; set; }

        public string SharerIP { get; set; }

        public string ViewerIP { get; set; }
    }
}