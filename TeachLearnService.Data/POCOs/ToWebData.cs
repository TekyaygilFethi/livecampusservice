﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TeachLearnService.Data.POCOs
{
    public class ToWebData
    {
        public int SharerID { get; set; }

        public int ViewerID { get; set; }

        public DateTime ConferenceStartTime { get; set; }

        public DateTime ConferenceEndTime { get; set; }
        
        public string Exception { get; set; }
    }
}
