//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace TeachLearnService.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class ConnectionTable
    {
        public int ID { get; set; }
        public string ConnectionString { get; set; }
        public string ConnectionName { get; set; }
        public string Password { get; set; }
        public Nullable<int> User_ID { get; set; }
        public Nullable<int> Sharer_ID { get; set; }
        public Nullable<int> Viewer_ID { get; set; }
        public string ViewerIP { get; set; }
        public string SharerIP { get; set; }
    
        public virtual UserTable UserTable { get; set; }
        public virtual UserTable UserTable1 { get; set; }
        public virtual UserTable UserTable2 { get; set; }
    }
}
